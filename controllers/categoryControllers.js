const Product = require('../models/Product');
const Category = require('../models/Category');
const auth = require('../auth');
const validate = require('../models/validator');
const Redis = require("ioredis");


const redis = new Redis('redis://red-clg3h8mg1b2c73a53i5g')

//***********************Get All categories***********************//
exports.getAllCategories = async (req, res) => {
	try{
		redis.get("categories", async(err, result) => {
			if(err){
				console.log(err)
			}
			if(result != null){
				console.log('has category cache result')
				return res.json(JSON.parse(result))
			}else{
				const categories = await Category.find().sort('name');
				await redis.set("categories", JSON.stringify(categories))
				res.send(categories);
			}
		})
	}catch(err){
		return res.status(500).send(err.message);
	}
};


//***********************Create Category***********************//
exports.addCategory = async (req, res) => {

const { error } = validate.validateCategory(req.body); 
 
if (error) return res.status(400).send(error.details[0].message);

const data = auth.decode(req.headers.authorization)

if(!data.isAdmin) return res.send('Unauthorized user');

	try{

		let isNameTaken = await Category.findOne({"name": req.body.name})

	 	if(isNameTaken) return res.send('Category name is already in use.')

		let category = new Category({
			
			imageUrl: req.body.imageUrl,
			name: req.body.name,
			
				})

  		category = await category.save();

  
  		return res.send(category);

	}catch(err){
		return res.status(500).send(err.message);
	}
};


//***********************Delete a Category***********************//
exports.deleteCategory = async (req, res) => {
	try{

  		const category = await Category.findByIdAndRemove(req.params.id);

  		if (!category) return res.status(400).send('Invalid categoryId.');

 		 return res.send(category);

	}catch(err){
	return res.status(500).send(err.message);
	}
};


//***********************Get a Specific Category***********************//
exports.getCategory = async (req, res) => {

	try{

		const category = await Category.findById(req.params.id);

  		if (!category) return res.status(404).send('The category with the given ID was not found.');


  		res.send(category);
	
	}catch(err){
		res.status(500).send(err.message);
	}

};



//***********************Get Products by Category***********************//

exports.getProductsByCategory = (req,res) => {

return Category.findById(req.params.categoryId)

			   .then(category => {

			   	if (!category) return res.status(400).send('Invalid category!')

			   		// console.log(category._id)

			   	return Product.find()

							  .and([{"category.categoryId": category._id},{isAvailable: true}])

			   		.then(product => {
			   			// console.log(product)

			   		if(!product) return res.status(400).send('Invalid product!')

			   		return res.send(product)	
			   		})

			   })

			   .catch(err => res.status(500).send(err.message));
			   
}








