const mongoose = require('mongoose');


const cartSchema = new mongoose.Schema({
    user: { 
      type: new mongoose.Schema({
          userId : {
            type:String,
            required: [true, "firstname name is required"]
          },
          email: {
            type: String,
            required: [true, "Email is required"]
          }
      }),
      required: true
    },
    products: [{
      type: new mongoose.Schema({
          productId: {
          type: String,
          required: true
        },
          name: {
          type: String,
          required: true,
        },
          price: {
          type: Number,
          required: true
        },
          quantity: {
          type: Number,
          default: 1
        },
          img:{
            type: String,
            required: true
        },
      }),
    }],
    totalAmount: {
     type: Number,
     default:0
    },
});


module.exports = mongoose.model('Cart', cartSchema);




