const Joi = require('@hapi/joi');


//validate user



module.exports.validateUser = (user) => {

const strongPasswordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
const stringPassswordError = "Password must be strong. At least one upper case alphabet. At least one lower case alphabet. At least one digit. At least one special character. Minimum eight in length"
// const fullNamePattern = /^[A-Z]+ [A-Z]+$/i;

  const schema = Joi.object({
    fullName: Joi.string().required().pattern(/^[A-Z]+ [A-Z]+$/i).uppercase()
              .messages({
              'any.required': 'Fullname is required.',
              'string.pattern.base': 'Firstname and Lastname should be separated by a space.',
              'string.empty': 'This field should not be empty.'
              }),
    email: Joi.string().min(10).max(255).required().email()
              .messages({
              'any.required': 'Email is required.',
              'string.pattern.base': 'Please input a valid email',
              'string.empty': 'This field should not be empty.'
              }),
    mobileNo: Joi.string().required().length(11).pattern(/^[0-9]+$/)
              .messages({
              'any.required': 'Mobile number is required.',
              'string.empty': 'This field should not be empty.',
              'string.length': 'Mobile number should be 11 digits and should follow this pattern 09XX-XXXX-XXX.'
    }),
    password: Joi.string().required().pattern(strongPasswordRegex)
              .messages({
                'any.required': 'Password is required.',
                'string.empty': 'This field should not be empty',
                'string.pattern.base': `${stringPassswordError}`
              }),
    confirmPassword: Joi.string().valid(Joi.ref('password')).required()
                      .messages({
                        'any.only': 'Password does not match.',
                        'string.empty': 'This field should not be empty',
                        'any.required': 'Confirm Password field is required'
                      })
  });

  return schema.validate(user);
}

// //validate course
// module.exports.validateCourse = (course) => {
//   const schema = Joi.object({
//     name: Joi.string().required(),
//     description: Joi.string().required(),
//     price: Joi.number().required()
//   });

//   return schema.validate(course);
// }


//validate user login
module.exports.validateLogin = (login) => {
  const schema = Joi.object({
        email: Joi.string().min(10).max(255).required().email()
              .messages({
              'any.required': 'Email is required.',
              'string.pattern.base': 'Please input a valid email',
              'string.empty': 'This field should not be empty.'
              }),
    password: Joi.string().required()
  });

  return schema.validate(login);
}


//validate category
module.exports.validateCategory = (category) => {
  const schema = Joi.object({
    name: Joi.string().required().uppercase()
          .messages({
          'any.required': 'Category name is required.',
          'string.empty': 'This field should not be empty.'
          }),
  });

  return schema.validate(category);
}





