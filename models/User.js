const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

  fullName : {
    type:String,
    required: [true, "fullName name is required"],
    uppercase: true
  },
  email: {
    type: String,
    required: [true, "Email is required"],
    lowercase: true
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile no is required"]
  },
  password: {
    type: String,
    required: [true, "Password is required"]
  },
  confirmPassword:{
    type: String,
    required: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
})

module.exports = mongoose.model('User', userSchema);


