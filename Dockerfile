FROM redis:latest

# Expose Redis default port
EXPOSE 6379

CMD ["redis-server"]