const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');
const cartRoutes = require('./routes/cartRoutes');
const categoryRoutes = require('./routes/categoryRoutes');
const stripeRoute = require("./routes/stripe");
const fs = require('fs/promises');
const path = require('path');
const axios = require('axios');

// Environment Variables
dotenv.config();
const connectionString = process.env.CONNECTION_STRING;


const app = express();
const port = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Database Connect
mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Successfully connected to MongoDB"))
  .catch(err => console.error("Connection error:", err));

// Route middleware
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/carts', cartRoutes);
app.use('/categories', categoryRoutes);
app.use("/checkout", stripeRoute);

// Heroku testing
const LINE_TOKEN_URL = 'https://api.line.me/oauth2/v2.1/token';
const LINE_PROFILE_URL = 'https://api.line.me/v2/profile';

app.get('/callback', async (req, res) => {
  const code = req.query.code;
  const filePath = path.join(__dirname, 'output.txt');

  try {
      const codeVerifier = await fs.readFile(filePath, { encoding: 'utf-8' });

      console.log('checking code verifier.......', codeVerifier)
      console.log('checking type.....', typeof codeVerifier)

      const tokenParams = new URLSearchParams({
          grant_type: 'authorization_code',
          code,
          redirect_uri: 'https://bikada-bikeshop.onrender.com/callback',
          client_id: '2003389325',
          client_secret: 'd01291cb13b2b595fefbee20fb5d6af4',
          code_verifier: codeVerifier
      }).toString();

      const response = await axios.post(LINE_TOKEN_URL, tokenParams, {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      const accessToken = response.data.access_token;
      const userProfile = await axios.get(LINE_PROFILE_URL, {
          headers: { 'Authorization': `Bearer ${accessToken}` }
      });

      res.redirect(`exp://u3pksho.orfenoko.8081.exp.direct/--/login?data=${encodeURIComponent(JSON.stringify(userProfile.data))}`);
  } catch (error) {
      console.error('Error during LINE login:', error);
      res.status(500).send(`Server error: ${error.toString()}`);
  }
});

// Existing POST endpoint to save data
app.post('/saveCode', async (req, res) => {
  const { data } = req.body;
  if (!data) {
      return res.status(400).send('No data provided');
  }

  try {
      await fs.writeFile('output.txt', data);
      res.send('Data written to file successfully');
  } catch (err) {
      console.error('Error writing to file:', err);
      res.status(500).send('Failed to write to file');
  }
});

// Error handlers
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.send(`Error: ${error.message}`);
});



app.listen(port, () => console.log(`Server running at port ${port}`));
