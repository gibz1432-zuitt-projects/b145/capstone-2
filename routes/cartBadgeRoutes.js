const express = require('express');
const router = express.Router();
const cartBadgeController = require('../controllers/cartBadgeControllers')
const auth = require('../auth');


//Create an order
router.post('/', auth.verify, cartBadgeController.addCartBadge);


module.exports = router;