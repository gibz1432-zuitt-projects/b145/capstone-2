const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderControllers')
const auth = require('../auth');


//Create an order
router.post('/', auth.verify, orderController.addOrder);

//retrieval all orders
router.get('/all', auth.verify,  orderController.getAllOrders);
	
// retrive authenticated user's orders
router.get('/', auth.verify, orderController.getUserOrders);


//Test an order
router.post('/testOrder', auth.verify, orderController.testOrder);





//retrieval of specific product
// router.get('/:productId', productController.getProduct);


//update product information
// router.put('/:productId', auth.verify, productController.updateProduct);




//archiving a product

// router.put('/:productId/archive', auth.verify, productController.archiveProduct);


//add course enrollee

// router.post('/:courseId/enroll', auth.verify, courseController.enrollCourse)

module.exports = router;



